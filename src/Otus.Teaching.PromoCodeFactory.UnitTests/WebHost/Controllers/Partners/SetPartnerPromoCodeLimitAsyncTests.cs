﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Collections.Generic;
using System;
using Xunit;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.TestsFixture;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.Global;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<SetPartnerPromoCodeLimitAsync_TestsFixture>
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly SetPartnerPromoCodeLimitRequest _partnersRepositoryRequestFixture;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests(SetPartnerPromoCodeLimitAsync_TestsFixture tests)
        {
            _partnersRepositoryMock = tests.PartnersRepositoryMock;
            _partnersRepositoryRequestFixture = tests.PartnersRepositoryRequestMock;
            _partnersController = tests.PartnersController;
        }

        /// <summary>
        /// 1.Если партнер не найден, то также нужно выдать ошибку 404; 
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            PartnerGlobal.SetupParnerGetById(_partnersRepositoryMock, partnerId);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _partnersRepositoryRequestFixture);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// 2.Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = PartnerGlobal.CreatePertnerAndSetupGetById(_partnersRepositoryMock, false);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _partnersRepositoryRequestFixture);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 3.Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_ZeroNumbersPromo()
        {
            // Arrange
            var partner = PartnerGlobal.CreatePertnerAndSetupGetById(_partnersRepositoryMock);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _partnersRepositoryRequestFixture);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);

        }

        /// <summary>
        /// 4.При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_CancelDateActivePromo()
        {
            // Arrange
            var partner = PartnerGlobal.CreatePertnerAndSetupGetById(_partnersRepositoryMock);

            var activeOldLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _partnersRepositoryRequestFixture);

            // Assert
            partner.PartnerLimits.FirstOrDefault(f => f.Id == activeOldLimit.Id).CancelDate.Should().NotBeNull();

        }

        /// <summary>
        /// 5.Лимит должен быть больше 0;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimitZero_ReturnsBadRequest()
        {
            // Arrange
            var partner = PartnerGlobal.CreatePertnerAndSetupGetById(_partnersRepositoryMock);

            var activeOldLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            //Ставлю лимит 0
            _partnersRepositoryRequestFixture.Limit = 0;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _partnersRepositoryRequestFixture);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 6.Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом);
        /// Если в текущей реализации найдутся ошибки, то их нужно исправить и желательно написать тест, чтобы они больше не повторялись.
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_SaveInDatabases()
        {
            // Arrange
            var partner = PartnerGlobal.CreatePertnerAndSetupGetById(_partnersRepositoryMock);

            var activeOldLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _partnersRepositoryRequestFixture);

            // Assert
            _partnersRepositoryMock.Verify(v => v.UpdateAsync(partner), Times.Once);

        }
    }
}