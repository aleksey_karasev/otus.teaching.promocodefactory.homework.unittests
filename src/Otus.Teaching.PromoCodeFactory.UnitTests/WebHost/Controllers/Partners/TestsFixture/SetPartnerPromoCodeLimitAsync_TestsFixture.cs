﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.TestsFixture
{
    public class SetPartnerPromoCodeLimitAsync_TestsFixture : IDisposable
    {
        public Mock<IRepository<Partner>> PartnersRepositoryMock { get; }
        public PartnersController PartnersController { get; }

        public SetPartnerPromoCodeLimitRequest PartnersRepositoryRequestMock { get; }

        public IServiceProvider ServiceProvider { get; set; }

        public IServiceCollection ServiceCollection { get; set; }


        public SetPartnerPromoCodeLimitAsync_TestsFixture()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            PartnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            PartnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

            PartnersRepositoryRequestMock = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            //var builder = new ConfigurationBuilder();
            //var configuration = builder.Build();
            //ServiceCollection = Configuration.GetServiceCollection(configuration, "Tests");
            //var serviceProvider = GetServiceProvider();
            //ServiceProvider = serviceProvider;
        }


        //private IServiceProvider GetServiceProvider()
        //{
        //    var serviceProvider = ServiceCollection
        //        .ConfigureInMemoryContext()
        //        .BuildServiceProvider();
        //    return serviceProvider;
        //}

        public void Dispose()
        {

        }
    }
}
