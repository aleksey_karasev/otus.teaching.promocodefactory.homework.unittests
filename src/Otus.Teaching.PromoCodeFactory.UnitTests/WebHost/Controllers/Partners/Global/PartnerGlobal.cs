﻿using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.Global
{
    public static class PartnerGlobal
    {

        /// <summary>
        /// Выполняет создание полностью заполненго партнера
        /// </summary>
        /// <param name="guid">Его гуид</param>
        public static Partner CreateBasePartner(Guid guid)
        {
            if(guid == Guid.Empty) { guid = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"); }

            var partner = new Partner()
            {
                Id = guid,
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        /// <summary>
        /// Выполняет Mock настройку GetById для теста
        /// </summary>
        /// <param name="p_partnersRepositoryMock"></param>
        /// <param name="p_partnerId"></param>
        /// <param name="p_returnPartner"></param>
        public static void SetupParnerGetById(Mock<IRepository<Partner>> p_partnersRepositoryMock,
                                            Guid p_partnerId,
                                            Partner p_returnPartner = null)
        {
            p_partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(p_partnerId))
                .ReturnsAsync(p_returnPartner);
        }

        /// <summary>
        /// Выполняет создание заполненного партнера, и выполняет mock настройку для GetById
        /// </summary>
        /// <param name="p_partnersRepositoryMock"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public static Partner CreatePertnerAndSetupGetById(Mock<IRepository<Partner>> p_partnersRepositoryMock, bool isActive = true)
        {
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner(partnerId);
            partner.IsActive = isActive;

            SetupParnerGetById(p_partnersRepositoryMock, partnerId, partner);

            return partner;
        }
    }
}
